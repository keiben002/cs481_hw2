﻿/************************
The purpose of this project is to create a basic calculator that 
uses multiplication, division, addition and subtraction between 
two numbers and displays the calculation.
*************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
//using System.Text.RegularExpressions;

namespace Basic_Calculator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]


    public partial class MainPage : ContentPage
    {

        private string @operator;       //holds the value of the operator

        private string[] nums = new string[2];    //creates a string array to hold the first number, second number and operator



        public MainPage()
        {
            InitializeComponent();

        }


        public void Button_Clicked(object sender, EventArgs e)
        {


            Button button = (Button)sender;

            if ("0123456789".Contains(button.Text)) //When the event for button is pressed for adding a number
            {
                addNumber(button.Text); //send data from specific button to addNumber() Function if numbers [0-9] are found.
            }
            else if ("x+-/".Contains(button.Text)) //When the event for button is pressed for adding an operator
            {
                Ops(button.Text); //sending operator to function OrderOfOps() if any of the specific operators are found.
            }
            else if ("=".Contains(button.Text)) //When the event for the equals symbol is pressed 
            {
                Calculate();    //send info to Calculate function to process math
            }
            else if ("CLR" == button.Text) //When the even for the CLR(Clear) is pressed 
            {
                Clear(this, null); //This is for clearing the array
            }
        }

        private void Calculate()
        {
            double? calc = 0;  //calculation result
            double? num1 = nums[0] == null ? null : (double?)double.Parse(nums[0]); //turns the first slot in the array to a double
            double? num2 = nums[1] == null ? null : (double?)double.Parse(nums[1]); //turns the second slot in the array to a double


            if (@operator == "+")
            {
                calc = num1 + num2;         //addition calculation
                result.Text = calc.ToString();
            }
            else if (@operator == "-")
            {
                calc = num1 - num2;         //subtraction calculation
                result.Text = calc.ToString();
            }
            else if (@operator == "/")
            {
                calc = num1 / num2;         //division calculation
                result.Text = calc.ToString();
            }
            else if (@operator == "x")
            {
                calc = num1 * num2;         //multiplication calculation
                result.Text = calc.ToString();
            }

            //the result.Text = calc.ToString(); displays calc as a string value to the text box

        }


        private void Ops(string op)
        {
            double? num1 = nums[0] == null ? null : (double?)double.Parse(nums[0]);     //num1 is set to a double value from a string       
            double? num2 = nums[1] == null ? null : (double?)double.Parse(nums[1]);     //num2 is set to a double value from a string

            if (op == "+") //case 1: This is hit if the character operator is '+' 
            {
                @operator = "+";
                //result.Text = num1.ToString() + " + " + num2.ToString();

            }
            else if (op == "-") //case 2: This is hit if the character operator is '-'
            {
                @operator = "-";
                //result.Text = num1.ToString() + " - " + num2.ToString();


            }
            else if (op == "x")  //case 3: This is hit if the character operator is 'x'
            {
                @operator = "x";
                //result.Text = num1.ToString() + " x " + num2.ToString();


            }
            else if (op == "/")  //case 4: This is hit if the character operator is '/'
            {
                @operator = "/";
                //result.Text = num1.ToString() + " / " + num2.ToString();


            }


        }

        private void addNumber(string num)
        {
            int index = @operator == null ? 0 : 1; //index for the nums[] string array, either going to be the first slot or the second slot

            nums[index] += num;        //depending on the index, the num will be added to the array slot 


            result.Text = num.ToString();   //result text box displays the number after it is added

            if(nums[0] != null && nums[1] != null)
            {
                result.Text = nums[0] + @operator + nums[1];    //this result is to show the string before the calculation
            }

        }

        private void Clear(object sender, EventArgs e)
        {
            nums[0] = null;                     //clearing the first number
            nums[1] = null;                     //clearing the second number
            @operator = null;                   //clearing the operator
            result.Text = "0";                  //display text as 0
        }


    }

}

